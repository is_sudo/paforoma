<?php
/*
Template Name:staff
*/
get_header(); ?>

        <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
        <div class="container main">
            <div class="row">
                    <?php
                    $args = [
                        'posts_per_page' => -1,
                        'orderby'        => 'menu_order',
                        'order'          => 'ASC',
                        'post_status'    => 'publish',
                        'post_type'      => 'cast'
                    ];
                    $posts = get_posts($args);
                    foreach ($posts as $post) :?>
                    <div class="col-xs-6">
                        <div class="thumbnail">
                            <a href="<?php echo get_permalink(); ?>">
                                <?php
                                $image = CFS()->get('photo_main');
                                if(empty($image)){ ?>
                                <img src="<?php bloginfo('template_url'); ?>/images/noimage.png" alt="<?php the_title();?>">
                                <?php } else { ?>
                                <img src="<?php echo $image;?>" alt="<?php the_title();?>">
                                <?php }; ?>
                            </a>
                            <div class="caption">
                            <h3><a href="<?php echo get_permalink(); ?>"><?php the_title();?></a></h3>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
            </div>
            <?php endwhile; endif; ?>

<?php get_footer(); ?>