<?php
/*
Template Name:service
*/
get_header(); ?>

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="container main">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<?php the_title(); ?>
							</div>
							<div class="panel-body wrapper_servise">
								<p class="course">ぱふぉろまコース</p>
								<p class="about">
									生ぱふぉが体感できちゃう業界初のコースです！<br />※お話や女の子に触れる事はできません。
								</p>
								<?php $services_pafo = CFS()->get('services_pafo');
								if(!empty($services_pafo)){
								foreach($services_pafo as $service_pafo): ?>

								<div class="inner">
									<ul>
										<li><?php echo $service_pafo['service_time'];?></li>
										<li><?php echo $service_pafo['service_price'];?></li>
										<li><?php echo $service_pafo['service_note'];?></li>
									</ul>
								</div>
									<?php endforeach; }; ?>

								<p class="course">ぱふぉリフレコース</p>
								<p class="about">お話しができてゆっくり楽しめちゃうコースです！</p>
								<?php $services_rihure = CFS()->get('services_rihure');
if(!empty($services_rihure)){
	foreach($services_rihure as $service_rihure): ?>

								<div class="inner">
									<ul>
										<li><?php echo $service_rihure['service_time'];?></li>
										<li><?php echo $service_rihure['service_price'];?></li>
										<li><?php echo $service_rihure['service_note'];?></li>
									</ul>
								</div>
								<?php endforeach; }; ?>
								<p class="note2">その他オプション各種多数ご用意しております♪</p>

							</div>
						</div>
					</div>
				</div>
				<?php endwhile; endif; ?>

					<?php get_footer(); ?>