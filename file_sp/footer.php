            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="list-group">
                      <a href="<?php bloginfo('url'); ?>" class="list-group-item<?php if(is_front_page()){ echo ' active'; }?>">ホーム</a>
                      <a href="<?php bloginfo('url'); ?>/whats" class="list-group-item<?php if(is_page('whats')){ echo ' active'; }?>">ぱふぉろまってこんなお店</a>
                      <a href="<?php bloginfo('url'); ?>/staff" class="list-group-item<?php if(is_page('staff') || is_singular('cast')){ echo ' active'; }?>">在籍生徒</a>
                      <a href="<?php bloginfo('url'); ?>/schedule" class="list-group-item<?php if(is_page('schedule')){ echo ' active'; }?>">出席表</a>
                      <a href="<?php bloginfo('url'); ?>/service" class="list-group-item<?php if(is_page('service')){ echo ' active'; }?>">料金</a>
                      <a href="<?php bloginfo('url'); ?>/access" class="list-group-item<?php if(is_page('access')){ echo ' active'; }?>">アクセス</a>
																					<a href="http://ameblo.jp/paforoma-akb/" target="_blank" class="list-group-item">ブログ</a>
                      <a href="<?php bloginfo('url'); ?>/recruit" class="list-group-item<?php if(is_page('rectuit')){ echo ' active'; }?>">女の子募集中</a>
                      <a href="<?php bloginfo('url'); ?>/shoplist" class="list-group-item<?php if(is_page('shoplist')){ echo ' active'; }?>">エフェクトグループ一覧</a>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="panel panel-default">
                          <div class="panel-body">
                            <dt>お問い合わせ</dt>
                            <dd><?php echo $cfs->get('phone_number', 50); ?></dd>
                            <dt>営業時間</dt>
                            <dd><?php echo $cfs->get('open_close', 50); ?></dd>
                            <dt>アクセス</dt>
																											<dd><?php echo $cfs->get('map', 62); ?></dd>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="copy"><small>c <?php bloginfo('name'); ?> All Rights Reserved.</small></p>
        </footer>

<?php wp_footer(); wp_footer(); echo "\n";//2回宣言しないと発動しない？?>

    </body>

</html>