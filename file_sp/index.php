<?php
/*
Template Name:index
*/
get_header(); ?>
        <div class="container main">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">

                    <?php $slides = $cfs->get('slider');
                    if(!empty($slides)){ ?>
                    <div class="index-main-image-fixed">
                        <?php if(!empty($slides[0]['slide_url'])) { ?>
                        <a href="<?php echo $slides[0]['slide_url'];?>"><img src="<?php echo $slides[0]['slide_image'];?>"></a>
                        <?php } else { ?>
                        <img src="<?php echo $slides[0]['slide_image'];?>"/>
                        <?php }; ?>
                    </div>
                    <?php }; ?>
                </div>
            </div>
            <div class="row">
                <a href="tel:<?php echo $cfs->get('phone_number', 50); ?>" class="index-phone-number">
                    <i class="glyphicon glyphicon-earphone"></i> <?php echo $cfs->get('phone_number', 50); ?>
                </a>
            </div>
            <div class="row">
                <ul class="index-main-manu">
                    <li><a href="<?php bloginfo('url'); ?>/staff">在籍表</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/schedule">出席表</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/service">料金</a></li>
                </ul>
            </div>

            <?php if(!empty($slides)){ ?>
            <div class="row">
                <!-- slider -->
                <ul class="bxslider">
                    <?php
                    foreach ($slides as $slide) :?>
                    <?php if(!empty($slide['slide_url'])) { ?>
                    <li><a href="<?php echo $slide['slide_url'];?>"><img src="<?php echo $slide['slide_image'];?>" /></a></li>
                    <?php } else { ?>
                    <li><img src="<?php echo $slide['slide_image'];?>" /></li>
                    <?php }; ?>
                    <?php endforeach;?>
                </ul>
            </div>
            <?php } ;?>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="panel panel-info">
                      <div class="panel-heading"><i class="glyphicon glyphicon-info-sign"></i> インフォメーション</div>
                      <div class="panel-body">
                        <?php
                        $post  = get_post('154');
                        echo $post->post_content;
                        ?>
                      </div>
                    </div>
                </div>
            </div>
<?php get_footer(); ?>