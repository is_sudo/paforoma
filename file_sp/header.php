<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
    <head>
        <title><?php wp_title ( '|', true,'right' ); ?><?php bloginfo('name'); ?></title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="<?php bloginfo("description"); ?>">
					<meta name="keywords" content="ぱふぉろま,ぱふぉリフレ,見学店,東京,秋葉原">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.png" type="image/vnd.microsoft.icon" />
					<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

        <!--OGP meta-->
        <meta property="og:title" content="<?php wp_title ( '|', true,'right' ); ?><?php bloginfo('name'); ?>">
        <meta property="og:type" content="website">
        <meta property="og:url" content="<?php bloginfo('url'); ?>">

        <?php
        wp_deregister_script('jquery');
        wp_head();  echo "\n"; ?>

    </head>
    <body>

        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>" class="logo"></a>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php bloginfo('url'); ?>">ホーム</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/whats">ぱふぉろまってこんなお店</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/staff">在籍生徒</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/schedule">出席表</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/service">料金</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/access">アクセス</a></li>
																							<li><a href="http://ameblo.jp/paforoma-akb/" target="_blank">ブログ</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/recruit">女の子募集中</a></li>
                      </ul>
                    </div>
                </div>
            </nav>
        </header>
