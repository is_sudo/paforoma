<?php
/*
Template Name:access
*/
get_header(); ?>

        <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
        <div class="container main">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><?php the_title(); ?></div>
                        <div class="panel-body">
                            <script type="text/javascript">
                                  var g = new google.maps.Geocoder(),
                                        map, center ;
                                    g.geocode({
                                        'address': '<?php echo CFS()->get('map');?>'
                                    }, function(results, status) {
                                        if (status == google.maps.GeocoderStatus.OK) {
                                            center = results[0].geometry.location;
                                            initialize();
                                        }
                                    });

                                    function initialize() {
                                        var options = {
                                            center: center,
                                            zoom: 17,
                                            mapTypeId: google.maps.MapTypeId.ROADMAP,
                                            scrollwheel: true,
                                            panControlOptions: {
                                                position: google.maps.ControlPosition.TOP_RIGHT
                                            },
                                            zoomControlOptions: {
                                                position: google.maps.ControlPosition.TOP_RIGHT
                                            },
                                            mapTypeControlOptions: {
                                                position: google.maps.ControlPosition.TOP_CENTER
                                            }
                                        };
                                        map = new google.maps.Map(document.getElementById('googlemap'), options);

                                        var marker = new google.maps.Marker({
                                            position: center,
                                            map: map
                                        });
                                    }
                            </script>
                            <div id="googlemap"></div>
																									<p><?php echo CFS()->get('map'); ?></p>
                            <?php the_content();?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; endif; ?>

<?php get_footer(); ?>