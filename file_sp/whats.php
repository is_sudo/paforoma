<?php
/*
Template Name:whats
*/
get_header(); ?>

        <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
        <div class="container main">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><?php the_title(); ?></div>
                        <div class="panel-body">
                            <h1><?php echo CFS()->get('information_title'); ?></h1>
																									<p class="comment"><?php echo CFS()->get('information_content'); ?></p>
                            <table class="table table-striped">
                                <tr>
                                    <th>営業時間</th>
                                </tr>
                                <tr>
                                    <td><?php echo CFS()->get('open_close'); ?></td>
                                </tr>
                                <tr>
                                    <th>定休日</th>
                                </tr>
                                <tr>
                                    <td><?php echo CFS()->get('close_day'); ?></td>
                                </tr>
                                <tr>
                                    <th>電話番号</th>
                                </tr>
                                <tr>
                                    <td><?php echo CFS()->get('phone_number'); ?></td>
                                </tr>
                            </table>
																									<div class="attention">
																									<h3>※注意事項※</h3>
                            <p><?php echo CFS()->get('attention'); ?></p>
																									<h3>※禁止事項※</h3>
                            <p><?php echo CFS()->get('ban'); ?></p>
																									</div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; endif; ?>

<?php get_footer(); ?>