<?php
/*
Template Name:staff
*/
get_header(); ?>

	<!-- main -->
	<div class="ui page grid stackable relaxed feature">
		<div class="row">

			<div class="twelve wide column" id="main">
				<!-- breadcrumb -->
				<div class="ui small breadcrumb">
					<a class="section" href="<?php bloginfo('url'); ?>">ホーム</a>
					<i class="right arrow icon divider"></i>
					<a class="section">
						<?php the_title(); ?>
					</a>
				</div>
				<!-- end breadcrumb -->

				<h2 class="ui red header">
					<?php the_title(); ?>
				</h2>

				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<div class="ui active tab stacked orange segment">
							<div class="staff-list">
								<?php
                            $args = [
                                'posts_per_page' => -1,
                                'orderby'        => 'menu_order',
                                'order'          => 'ASC',
                                'post_status'    => 'publish',
                                'post_type'      => 'cast'
                            ];
                            $posts = get_posts($args);
                            foreach ($posts as $post) :?>
									<div class="staff-block">
										<div class="staff-block-inner">
											<input type="hidden" name="url" value="<?php echo get_permalink(); ?>">
											<?php
                                    $image = CFS()->get('photo_main');
                                    if(empty($image)){ ?>
												<img src="<?php bloginfo('template_url'); ?>/images/noimage.png" alt="<?php the_title();?>" class="rounded ui image">
												<?php } else { ?>
													<img src="<?php echo $image;?>" alt="<?php the_title();?>" class="rounded ui image">
													<?php }; ?>
														<h3><?php the_title();?></h3>
										</div>
									</div>
									<?php endforeach; ?>
							</div>
						</div>
						<?php endwhile; endif; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<!-- end main -->
	<?php get_footer(); ?>