<?php
/*
Template Name:whats
*/
get_header(); ?>

    <!-- main -->
    <div class="ui page grid stackable relaxed feature">
        <div class="row">

            <div class="twelve wide column" id="main">
                    <!-- breadcrumb -->
                    <div class="ui small breadcrumb">
                      <a class="section" href="<?php bloginfo('url'); ?>">ホーム</a>
                      <i class="right arrow icon divider"></i>
                      <a class="section"><?php the_title(); ?></a>
                    </div>
                    <!-- end breadcrumb -->

                    <h2 class="ui red header">
                      <?php the_title(); ?>
                    </h2>

                    <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <div class="ui active stacked orange segment">
                        <h3 class="ui header wt_title">
                          <?php echo CFS()->get('information_title'); ?>
                        </h3>
                        <div class="ui grid stackable relaxed feature">
																									<div class="sixteen wide column wt_cont">
                                <p class="comment"><?php echo CFS()->get('information_content'); ?></p>

                                <div class="label">営業時間</div>
                                <p><small><?php echo CFS()->get('open_close'); ?></small></p>


                                <div class="label">定休日</div>
                                <p><small><?php echo CFS()->get('close_day'); ?></small></p>

                                <div class="label">電話番号</div>
                                <p><small><?php echo CFS()->get('phone_number'); ?></small></p>
                            </div>
                            <div class="six wide column">
                            </div>

                        </div>

                        <div class="attention_wrapper">
                            <div class="label">※注意事項※</div>
                            <p><small>
                            <?php echo CFS()->get('attention'); ?>
                            </small></p>

                            <div class="label">※禁止事項※</div>
                            <p><small>
                            <?php echo CFS()->get('ban'); ?>
                            </small></p>
                        </div>

                    </div>
                    <?php endwhile; endif; ?>

            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
    <!-- end main -->
<?php get_footer(); ?>