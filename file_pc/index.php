<?php
/*
Template Name:index
*/
get_header(); ?>
	<!-- slider -->
	<div class="ui page grid">
		<div class="wide column">

			<?php $slides = $cfs->get('slider');
                    if(!empty($slides)){ ?>
				<div class="top_baner">
					<?php if(!empty($slides[0]['slide_url'])) { ?>
						<a href="<?php echo $slides[0]['slide_url'];?>" target="_blank"><img src="<?php echo $slides[0]['slide_image'];?>"/></a>
						<?php } else { ?>
							<img src="<?php echo $slides[0]['slide_image'];?>" />
							<?php }; ?>
				</div>

				<ul class="bxslider">
					<?php
                    foreach ($slides as $slide) :?>
						<li>
							<?php if(!empty($slide['slide_url'])) { ?>
								<a href="<?php echo $slide['slide_url'];?>" target="_blank"><img src="<?php echo $slide['slide_image'];?>"/></a>
								<?php } else { ?>
									<img src="<?php echo $slide['slide_image'];?>" />
									<?php }; ?>
						</li>
						<?php endforeach;
                    } ;?>
				</ul>
		</div>
	</div>
	<!-- end slider -->

	<!-- main -->
	<div class="ui page grid stackable relaxed feature">
		<div class="row">

			<div class="twelve wide column" id="main">
				<div class="ui pointing secondary index-tab menu">
					<a class="active orange item" data-tab="first">本日の登校予定</a>
					<a class="blue item" data-tab="second">明日の予定</a>
					<?php /*<a class="green item" data-tab="third">明後日の予定</a> */ ?>
				</div>
				<?php
                    date_default_timezone_set('Asia/Tokyo');
                    $Ids = index_posts_ids();
                    $week = ['日','月','火','水','木','金','土'];
                    $params = [
                        0 => [
                            'tab' => 'first',
                            'color' => 'orange',
                        ],
                        1 => [
                            'tab' => 'second',
                            'color' => 'blue',
                        ],
                        /*2 => [
                            'tab' => 'third',
                            'color' => 'green',
                        ],*/
                    ];
                    $i = 0;
                    foreach ($params as $value):
                    ?>
					<div class="ui <?php echo ($i === 0)? 'active':'';?> tab stacked <?php echo $value['color'];?> segment" data-tab="<?php echo $value['tab'];?>">
						<?php if($i != 0){ ?>
							<div class="ui ribbon <?php echo $value['color'];?> label">
								<?php
                            $date = date("Y-m-d", strtotime("+{$i} day"));
                            $datetime = new DateTime($date);
                            $w = (int)$datetime->format('w');
                            echo date('Y年m月d日', strtotime($date)). '（'.$week[$w].'）';?>
							</div>
							<?php }; ?>
								<?php if($Ids[$i] != NULL) {
                        $post = get_post($Ids[$i]);
                        ?>

									<div class="staff-list">
										<?php $workers = CFS()->get('girls');
                            foreach ($workers as $post_id) :
                            $girl = get_post($post_id);
                            ?>
											<div class="staff-block">
												<div class="staff-block-inner">
													<input type="hidden" name="url" value="<?php echo get_permalink($post_id);?>">
													<?php
                                    $image = CFS()->get('photo_main',$post_id);
                                    if(empty($image)){ ?>
														<img src="<?php bloginfo('template_url'); ?>/images/noimage.png" alt="<?php echo $girl->post_title;?>" class="rounded ui image">
														<?php } else { ?>
															<img src="<?php echo $image;?>" alt="<?php echo $girl->post_title;?>" class="rounded ui image">
															<?php }; ?>
																<h3><?php echo $girl->post_title;?></h3>
												</div>
											</div>
											<?php endforeach;?>
									</div>
									<?php } else { ?>
										<p>準備中</p>
										<?php } ;?>
					</div>
					<?php
                    $i++;
                    endforeach; ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
	<!-- end main -->
	<?php get_footer(); ?>