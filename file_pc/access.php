<?php
/*
Template Name:access
*/
get_header(); ?>

    <!-- main -->
    <div class="ui page grid stackable relaxed feature">
        <div class="row">

            <div class="twelve wide column" id="main">
                    <!-- breadcrumb -->
                    <div class="ui small breadcrumb">
                      <a class="section" href="<?php bloginfo('url'); ?>">ホーム</a>
                      <i class="right arrow icon divider"></i>
                      <a class="section"><?php the_title(); ?></a>
                    </div>
                    <!-- end breadcrumb -->

                    <h2 class="ui red header">
                      <i class="map marker icon"></i>
                      <?php the_title(); ?>
                    </h2>

                    <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <div class="ui active stacked orange segment">
                        <script type="text/javascript">
                              var g = new google.maps.Geocoder(),
                                    map, center ;
                                g.geocode({
                                    'address': '<?php echo CFS()->get('map');?>'
                                }, function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        center = results[0].geometry.location;
                                        initialize();
                                    }
                                });

                                function initialize() {
                                    var options = {
                                        center: center,
                                        zoom: 17,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        scrollwheel: true,
                                        panControlOptions: {
                                            position: google.maps.ControlPosition.TOP_RIGHT
                                        },
                                        zoomControlOptions: {
                                            position: google.maps.ControlPosition.TOP_RIGHT
                                        },
                                        mapTypeControlOptions: {
                                            position: google.maps.ControlPosition.TOP_CENTER
                                        }
                                    };
                                    map = new google.maps.Map(document.getElementById('googlemap'), options);

                                    var marker = new google.maps.Marker({
                                        position: center,
                                        map: map
                                    });
                                }
                        </script>
                        <div id="googlemap"></div>
																					<?php echo CFS()->get('map'); ?>

                        <?php the_content();?>
                    </div>
                    <?php endwhile; endif; ?>
													
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
    <!-- end main -->
<?php get_footer(); ?>