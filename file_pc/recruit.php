<?php
/*
Template Name:recruit
*/
get_header(); ?>

    <!-- main -->
    <div class="ui page grid stackable relaxed feature">
        <div class="row">

            <div class="twelve wide column" id="main">
                    <!-- breadcrumb -->
                    <div class="ui small breadcrumb">
                      <a class="section" href="<?php bloginfo('url'); ?>">ホーム</a>
                      <i class="right arrow icon divider"></i>
                      <a class="section"><?php the_title(); ?></a>
                    </div>
                    <!-- end breadcrumb -->

                    <h2 class="ui red header">
                      <i class="inbox icon"></i>
                      <?php the_title(); ?>
                    </h2>

                    <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <div class="ui active stacked orange segment">

                            <?php the_content();?>

                        <table class="ui table segment celled">
                            <tbody>
                                <tr>
                                    <td>勤務地</td>
                                    <td><?php echo CFS()->get('rectuit_place');?></td>
                                </tr>
                                <tr>
                                    <td>年齢</td>
                                    <td><?php echo CFS()->get('recruit_age');?></td>
                                </tr>
                                <tr>
                                    <td>勤務時間</td>
                                    <td><?php echo CFS()->get('recruit_work_time');?></td>
                                </tr>
                                <tr>
                                    <td>給与</td>
                                    <td><?php echo CFS()->get('recruit_salary');?></td>
                                </tr>
                                <tr>
                                    <td>待遇</td>
                                    <td><?php echo CFS()->get('recruit_treatment');?></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="ui form segment">
                        <?php echo do_shortcode( '[contact-form-7 id="67" title="求人"]' ); ?>
                        </div>
                    </div>
                    <?php endwhile; endif; ?>

            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
    <!-- end main -->
<?php get_footer(); ?>