<!DOCTYPE html>
<html>
<head>
    <!-- Site Properities -->
    <title><?php wp_title ( '|', true,'right' ); ?><?php bloginfo('name'); ?></title>

    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="<?php bloginfo("description"); ?>">
    <meta name="keywords" content="ぱふぉろま！,Parforoma,見学店,東京,秋葉原">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.png" type="image/vnd.microsoft.icon" />
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
    

    <!--OGP meta-->
    <meta property="og:title" content="<?php wp_title ( '|', true,'right' ); ?><?php bloginfo('name'); ?>">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php bloginfo('url'); ?>">

    <?php
    wp_deregister_script('jquery');
    wp_head();  echo "\n"; ?>

    <script src="<?php bloginfo('template_url'); ?>/js/modernizr.custom.46884.js"></script>

</head>
<body <?php body_class(); ?>>
    <!-- header -->
    <div class="ui page grid stackable relaxed feature head_line">
        <div class="row">
									<div class="eight wide column">
                <h1 id="logo"><a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>"></a></h1>
            </div>
            <div class="eight wide column right aligned">
                <div id="head-info" class="clearfix">
                    <dl>
																					<dt><i class="fa fa-phone fa-fw"></i>お問い合わせ</dt>
                        <dd class="clearfix"><?php echo $cfs->get('phone_number', 50); ?></dd>
																					<dt><i class="fa fa-clock-o fa-fw"></i>営業時間</dt>
                        <dd><?php echo $cfs->get('open_close', 50); ?></dd>
																					<dt><i class="fa fa-map-marker fa-fw"></i>アクセス</dt>
																					<dd><?php echo $cfs->get('map', 62); ?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
    <!-- end header -->