<div class="four wide column" id="menu">
	<div class="ui fluid vertical menu" id="origin_menu">
		<a class="<?php if(is_front_page()){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>">
			<i class="fa fa-star"></i> ホーム
		</a>
		<a class="<?php if(is_page('staff') || is_singular('cast')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/staff">
			<i class="fa fa-star"></i> 在籍生徒一覧
		</a>
		<a class="<?php if(is_page('whats')){ echo 'active ';}?>item orange" href="<?php bloginfo('url'); ?>/whats">
			<i class="fa fa-star"></i> ぱふぉろま<br>&nbsp;&nbsp;&nbsp;&nbsp;ってこんなお店
		</a>
		<a class="<?php if(is_page('information')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/information">
			<i class="fa fa-star"></i> インフォメーション
		</a>
		<a class="<?php if(is_page('service')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/service">
			<i class="fa fa-star"></i> サービス・料金
		</a>
		<a class="<?php if(is_page('access')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/access">
			<i class="fa fa-star"></i> アクセス
		</a>
		<?php /*
                    <a class="item orange" href="event.html">
                        <i class="fa fa-star"></i>
                        イベント
                    </a> */ ?>
		<a class="item orange" href="http://ameblo.jp/paforoma-akb/" target="_blank">
				<i class="fa fa-star"></i> ブログ
			</a>
			<?php /*
                    <a class="item orange" href="reserve.html">
                        <i class="fa fa-star"></i>
                        予約
                    </a> */ ?>
				<a class="<?php if(is_page('recruit')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/recruit">
					<i class="fa fa-star"></i> 女の子募集中
				</a>
				<a class="<?php if(is_page('shoplist')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/shoplist">
					<i class="fa fa-star"></i> エフェクトグループ一覧
				</a>
	</div>
</div>


<div id="sp_menu_open"><i class="star icon"></i><span>MENU</span></div>
<div class="ui fluid vertical menu" id="sp_menu">
	<a class="<?php if(is_front_page()){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>">
		<i class="fa fa-star"></i> ホーム
	</a>
	<a class="<?php if(is_page('staff') || is_singular('cast')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/staff">
		<i class="fa fa-star"></i> 在籍生徒一覧
	</a>
	<a class="<?php if(is_page('whats')){ echo 'active ';}?>item orange" href="<?php bloginfo('url'); ?>/whats">
		<i class="fa fa-star"></i> ぱふぉろま
		<br>ってこんなお店
	</a>
	<a class="<?php if(is_page('information')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/information">
		<i class="fa fa-star"></i> インフォメーション
	</a>
	<a class="<?php if(is_page('service')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/service">
		<i class="fa fa-star"></i> サービス・料金
	</a>
	<a class="<?php if(is_page('access')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/access">
		<i class="fa fa-star"></i> アクセス
	</a>
	<?php /*
                    <a class="item orange" href="event.html">
                        <i class="fa fa-star"></i>
                        イベント
                    </a> */ ?>
		<a class="item orange" href="http://ameblo.jp/lovelife-kengaku/" target="_blank">
			<i class="fa fa-star"></i> ブログ
		</a>
		<?php /*
                    <a class="item orange" href="reserve.html">
                        <i class="fa fa-star"></i>
                        予約
                    </a> */ ?>
			<a class="<?php if(is_page('recruit')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/recruit">
				<i class="fa fa-star"></i> 女の子募集中
			</a>
			<a class="<?php if(is_page('shoplist')){ echo 'active '; }?>item orange" href="<?php bloginfo('url'); ?>/shoplist">
				<i class="fa fa-star"></i> エフェクトグループ一覧
			</a>
			<a class="item orange" id="sp_menu_close"><i class="remove icon"></i> Close</a>
</div>