/*

	drag
	sort
	
	tabale



	div id=sortable
		table
			tr.sort_record #id
	input type=hidden  name=sort_record_data  id=sort_record_data value

	drag時に value を置き換える
	
*/
// flgDragMemo
var flgDragMemo=0;
function dragMemo(){
	$('.drag-memo').html('<a>.<br>順番は「並び順変更する」ボタンを押すと<br>決定します<br> .</a>');
	$('.drag-memo-ver2').html('<a>.<br>順番は「更新する」ボタンを押すと<br>決定します<br> .</a>');
}

// id list
var idList = new Array();

function getSortRecord_id() {
	idList.length = 0;

	var tr = $('#sortable table tbody tr.sort_record'); //アクティブのみ取得
	for (var i = 0, l = tr.length; i < l; i++) {
		idList.push(tr.eq(i).attr('id'));
	}
	changeNumber();
	hiddenValueInput(idList);
}

// 順番変更
function changeNumber() {
	var tr = $('#sortable table tbody tr.sort_record'); //アクティブのみ取得
	for (var i = 0, l = tr.length; i < l; i++) {
		var num = i + 1;
		var cells = tr.eq(i).find('td:nth-child(1)').text(num);
	}
}

// hidden inputのvalue を変更 arr:並び変え対象のid
function hiddenValueInput(arr) {
	$(':hidden[name="sort_record_data"]').val(arr);
}

// ドラッグ操作の補助
function helper1(e, tr) {
	var $originals = tr.children();
	var $helper = tr.clone();
	$helper.children().each(function (index) {
		$(this).width($originals.eq(index).width());
	});
	return $helper;
}



// 実行
$(window).load(function () {
	$('#sortable table tbody').sortable({
		//以下参考　http://kamegu.hateblo.jp/entry/jquery-ui/sortable-table2
		//http://stacktrace.jp/jquery/ui/interaction/sortable.html
		helper: helper1,
		cursor: "s-resize",
		opacity: 0.7,
		axis: 'y',
		placeholder: 'highlight',
		items: '> tr:not(.deactivated,.layout-fix)',
		update: function (ev, tr) {
			getSortRecord_id();
			if(flgDragMemo == 0){
				flgDragMemo = 1;
				dragMemo();
			}
		},
		stop: function (ev, tr) {

		}
	});
});