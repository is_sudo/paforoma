$(document).ready(function () {

//  Self-destruct
	setTimeout('$(".vanish3").html("");', 3000);

//
    $('.collapse dt').click(function () {
        $(this).next('dd').slideToggle();
        $(this).next('dd').siblings('dd').slideUp();
        $(this).toggleClass("active");
        $(this).toggleClass('open');
        $(this).siblings('dt').removeClass('open');
    });

        $('.collapse h2').click(function () {
        $(this).next('ul').slideToggle();
        $(this).next('ul').siblings('ul').slideUp();
        $(this).toggleClass("active");
        $(this).toggleClass('open');
        $(this).siblings('h2').removeClass('open');
    });


//back-top Button
	// hide #back-top first
	$("#back-top").hide();

	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			// distance
			if ($(this).scrollTop() > 180) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

	//Owl Carousel
	$(".owl-carousel").owlCarousel({
		navigation : true,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem : true,
		autoPlay : 3000,
		stopOnHover :true,
	});


	
//携帯の時だけ電話のリンク有効だ
 	$(function(){
    	var ua = navigator.userAgent;
    	if(ua.indexOf('iPhone') > 0 || ua.indexOf('Android') > 0){
        	$('.tel-link').each(function(){
            	var str = $(this).text();
            	$(this).html($('<a>').attr('href', 'tel:' + str.replace(/-/g, '')).append(str + '</a>'));
        	});
    	}
	});


});