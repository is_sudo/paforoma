<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"><![endif]-->
<!--[if IE 9]><html class="ie ie9"><![endif]-->
<!--[if !IE]><!-->
<html lang="ja">
<!--<![endif]-->
<head>
<?php if(isset($include_meta)) echo $include_meta; ?>
<?php if(isset($include_css)) echo $include_css; ?>
<?php if(isset($include_ga)) echo $include_ga; ?>
<?php if(isset($include_js)) echo $include_js; ?>
</head>

<body>

<?php if(isset($contents_block)) echo $contents_block; ?>

	<!--meanmenu　レスポンシブ用メニュー設定-->
	<script src="/js/jquery.meanmenu.js"></script>
	<script>
		jQuery(document).ready(function () {
    	jQuery('.mobile header nav').meanmenu();
    		$("a.meanmenu-reveal").click(function(){
        	$(".mean-bar").toggleClass("open");		
				})	 
		});
	</script>
<!--Device.js-->
<script src="/js/device.js"></script>
<script>
    console.log("device.portrait() === %s", device.portrait());
    console.log("device.landscape() === %s", device.landscape());
    console.log("device.mobile() === %s", device.mobile());
    console.log("device.tablet() === %s", device.tablet());
    console.log("device.ipad() === %s", device.ipad());
    console.log("device.ipod() === %s", device.ipod());
    console.log("device.iphone() === %s", device.iphone());
    console.log("device.android() === %s", device.android());
    console.log("device.androidTablet() === %s", device.androidTablet());
    console.log("device.blackberryTablet() === %s", device.blackberryTablet());
</script>

</body>
</html>